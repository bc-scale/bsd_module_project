locals {
  labels = {
    tf : "bsd_module_project"
  }
  project_folder_id = var.folder_id != "" ? var.folder_id : null
  temp_project_name = var.environment_suffix == "" ? var.project_name : "${var.project_name}-${var.environment_suffix}"
  full_project_name = "${var.company_prefix}-${local.temp_project_name}"
  enabled_apis = [
    "compute.googleapis.com",
    "iam.googleapis.com",
    "cloudbilling.googleapis.com",
    "storage-component.googleapis.com",
    "storage-api.googleapis.com",
    "serviceusage.googleapis.com",
    "bigquery.googleapis.com",
    "admin.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "appengine.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "cloudbuild.googleapis.com",
    "artifactregistry.googleapis.com",
    "secretmanager.googleapis.com",
    "container.googleapis.com",
    "run.googleapis.com",
    "cloudfunctions.googleapis.com",
    "containeranalysis.googleapis.com"
  ]
}


module "bsd-project" {
  source                  = "terraform-google-modules/project-factory/google"
  version                 = "~> 13.0"
  name                    = local.full_project_name
  random_project_id       = false
  org_id                  = var.org_id
  billing_account         = var.billing_id
  svpc_host_project_id    = var.vpc_host
  labels                  = merge(var.bsd_labels, local.labels)
  create_project_sa       = true
  default_service_account = "deprivilege"
  folder_id               = local.project_folder_id
  activate_apis           = local.enabled_apis
}
resource "google_project_service" "enabled_api" {
  depends_on = [
    module.bsd-project
  ]
  for_each                   = toset(var.extra_apis)
  project                    = module.bsd-project.project_id
  service                    = each.value
  disable_dependent_services = true
}

module "mandatory-alerts" {
  depends_on = [
    module.bsd-project,
    google_project_service.enabled_api
  ]
  source     = "git::https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_metric_alerts.git//?ref=tags/v0.1.1"
  project_id = module.bsd-project.project_id
}

