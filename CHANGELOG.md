# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.3](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.2.3) - 2022-09-12

### Fixed
- Keeps the service accounts - esp for app engine etc
## [1.2.2](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.2.2) - 2022-08-27

### Changed
- Updates gpf to version 13

## [1.2.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.2.1) - 2022-08-25

### Changed
- updated developer tools to version 1
## [1.2.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.2.0) - 2022-01-20
### Added
- cloudfunctions.googleapis.com
- containeranalysis.googleapis.com

## [1.1.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.1.0) - 2022-01-20
### Added
- enable `run.googleapis.com` by default

## [1.0.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v1.0.0) - 2022-01-15

### Added
- Changes module source to https as modules now all public

## [0.1.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v0.1.0) - 2021-12-29

### Added
- Updated metric alerts version to 0.1.0

- Initial release
## [0.0.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/releases/v0.0.1) - 2021-03-21

### Features

- Initial release
